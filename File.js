const Promise = require("promise");

module.exports = class File
{

    constructor(file)
    {
        this.__file = file;
    }

    _load(content)
    {
        return new Promise((resolve, reject) => {
            fs.access(this.__file, error => { // Check if the file exists
                if (error)
                {
                    // File doesn't exists
                    this.content = content;

                    this.write()
                        .then(resolve)
                        .catch(reject);
                }
                else
                {
                    // File exists
                    this.read()
                        .then(resolve)
                        .catch(reject);
                }
            });
        });
    }

    read()
    {
        return new Promise((resolve, reject) => {
            fs.readFile(this.__file, (error, data) => {
                if (error)
                {
                    reject(error);
                }
                else
                {
                    const content = data.toString();

                    try {
                        this.content = JSON.parse(content);
                    } catch (error) {
                        this.content = content;
                    }

                    resolve(this);
                }
            });
        });
    }

    write()
    {
        return new Promise((resolve, reject) => {
            fs.writeFile(this.__file, this.content, error => {
                if (error)
                {
                    reject(error);
                }
                else
                {
                    resolve(this);
                }
            });
        });
    }

}