const Directory = require("./Directory.js");

module.exports = function (object)
{
    return (new Directory(process.cwd()))._load(object);
}