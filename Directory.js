const fs = require("fs");
const path = require("path");

const Promise = require("promise");

const File = require("./File.js");

module.exports = class Directory
{
    
    constructor(directory)
    {
        this.__directory = directory;
    }

    __checkDirectory()
    {
        return new Promise((resolve, reject) => {
            fs.access(this.__directory, error => {
                if (error)
                {
                    fs.mkdir(this.__directory, error => {
                        if (error)
                        {
                            reject(error);
                        }
                        else
                        {
                            resolve(false);
                        }
                    });
                }
                else
                {
                    resolve(true);
                }
            });
        });
    }

    async __loadDirectory()
    {
        return new Promise(async (resolve, reject) => {
            fs.readdir(this.__directory, (error, files) => {
                if (error)
                {
                    reject(error);
                }
                else
                {
                    for (const name of files)
                    {
                        const file = path.join(this.__directory, name);

                        this.files[basename] = await (new File(file)).read();
                    }
                }
            });
        });
    }

    async __loadObject(object)
    {
        for (const name in object)
        {
            const content = object[name];
            const file = path.join(this.__directory, name);
            const basename = path.basename(name, path.extname(name));

            if (path.extname(name) || content instanceof String)
            {
                this.files[basename] = await (new File(file))._load(content);
            }
            else
            {
                this.files[basename] = await (new Directory(file))._load(content);
            }
        }
    }

    async _load(object)
    {
        return new Promise(async (resolve, reject) => {
            if (await this.__checkDirectory())
            {
                await this.__loadDirectory();
            }
            
            await this.__loadObject(object);

            resolve(this);
        });
    }

    async read()
    {
        return new Promise(async (resolve, reject) => {
            for (const file of Object.values(this.files))
            {
                await file.read();
            }

            resolve(this);
        });
    }

    async write()
    {
        return new Promise(async (resolve, reject) => {
            for (const file of Object.values(this.files))
            {
                await file.write();
            }

            resolve(this);
        });
    }

    contents()
    {
        const output = {};

        for (const basename of this.files)
        {
            const file = this.files[basename];

            if (file instanceof Directory)
            {
                output[basename] = file.contents();
            }
            else if (file instanceof File)
            {
                output[basename] = file.content;
            }
        }

        return output;
    }

}